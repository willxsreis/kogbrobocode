package kog;
import robocode.*;
import java.awt.Color;

/**
 * KogB - a robot by Wiliam Reis
 * Proposta: Criar uma robô que ande pelas beiradas, e atire sempre que encontrar um inimigo em seu caminho!
 */
public class KogB extends Robot
{
		boolean alerta; // criando uma variável e a incializando, serve para alerta de robôs na área para movimentar-se
		double xy;
	/**
	 * run: KogB's default behavior
	 */
	public void run(){
	
		xy = (getBattleFieldWidth() + getBattleFieldHeight() );
		alerta=false;
		turnLeft(getHeading() % 90); //retornando a direção do adversário de acordo a tela, VERIFICAR SE ÂNGULO ESTÁ CERTO
		ahead(xy);
		alerta= true; // mudando o valor da variável boolean
		turnGunRight(90); // Revisar ângulo
		turnRight(90); // Revisar ângulo
		 /** 
		  * LEMBRETE: Não posso aplicar angulos por causa do desafio da Solutis, pois não colocaram o tamanho do mapa
		  */

	
		// Robot main loop
		while(true){
			// Criar um bloco de instruções para que em toda a partida o robô esteja andando as redor do campo
			alerta=true;
			ahead(xy); //LEMBRETE: Não posso pôr ângulo
			alerta=false;
			turnRight(90); //Revisar
			
		}
	}
	
	public void onHitByBullet(HitByBulletEvent e) {
		//Replace the next line with any behavior you would like
		back(100);
	}
	
	public void onHitRobot(HitRobotEvent batida) {
			if(batida.getBearing()> -90 && batida.getBearing()<90){ //Calcular de o robô está a frente ou não e realizar uma instrução 
			back(100);
		}else{
			ahead(100); //Caso não esteja atrás
		}
	}
	public void onScannedRobot(ScannedRobotEvent fireHour){
		fire(1);
	}
}	
	
	//}
